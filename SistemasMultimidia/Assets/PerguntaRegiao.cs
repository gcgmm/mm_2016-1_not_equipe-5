﻿using UnityEngine;
using System.Collections;

public class PerguntaRegiao : MonoBehaviour {

    public Transform brasil;
    public Transform resposta;
    public Transform objetoResposta;

    void OnEnable()
    {     
        Transform[] objects = brasil.GetComponentsInChildren<Transform>();

        foreach (Transform to in objects)
        {
            if ((to.parent != null) && (to.parent.parent != null))
            {
                Transform t = to.parent.parent.GetComponent<Transform>();

                if (t == resposta)
                {
                    to.tag = "RespostaRegiao";
                }
                else {
                    to.tag = "RespostaErrada";
                }
            }
        }
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
