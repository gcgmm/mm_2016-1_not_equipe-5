﻿using UnityEngine;
using System.Collections;

public class Responder : MonoBehaviour {

    private bool canAsnwer;
    private bool canBack;
    private Transform estadoT;
    private Estado estado;
    private Regiao regiao;
    private float distance;
    private bool getBack;
	private bool running = false;
    private Vector3 origem;
    private Vector3 escalaOrigem;
    private Vector3 escalaDestino;
    public Material defaultMaterial;
    public Material materialRespotaErrada;
    public Material materialRespotaCerta;
    public Material materialResposta1;
    public Material materialResposta2;
    public Material materialResposta3;
    public Material materialResposta4;
    public Material materialResposta5;
    public GameObject textRespostaCerta;
    public GameObject textRespostaErrada;
    public GameObject perguntasContainer;
    public GameObject brasil;
    public int start;
    private Transform[] perguntas;
    private int perguntaAtual;
    private int materialAtual;
    private Material[] materiais = new Material[5];
    public InicializacaoMapa scriptInicializacao;
    private GameObject objetoResposta;
    private AudioSource audio;
    public AudioClip correto;
    public AudioClip errado;

    // Use this for initialization
    void Start () {
        canAsnwer = true;       
        getBack = false;
        textRespostaCerta.SetActive(false);
        textRespostaErrada.SetActive(false);
        setMaterial(brasil, defaultMaterial);
        perguntas = new Transform[perguntasContainer.transform.childCount];

        for (int i = 0; i < perguntasContainer.transform.childCount; i++)
        {
            perguntas[i] = perguntasContainer.transform.GetChild(i);
        }

        shuffleTransforms(perguntas, 0, 4);
        shuffleTransforms(perguntas, 5, perguntas.Length - 1);

        perguntaAtual = 0;
        audio = GetComponent<AudioSource>();
    }

    void shuffleTransforms(Transform[] transforms, int beginIndex, int endIndex)
    {
        for (int t = beginIndex; t < endIndex; t++)
        {
            Transform tmp = transforms[t];            
            int r = Random.Range(beginIndex, endIndex + 1);
            transforms[t] = transforms[r];
            transforms[r] = tmp;
        }
    }

    void inicializarPerguntas()
    {        
        perguntas[perguntaAtual].gameObject.SetActive(true);        
        materialAtual = 0;
        materiais[0] = materialResposta1;
        materiais[1] = materialResposta2;
        materiais[2] = materialResposta3;
        materiais[3] = materialResposta4;
        materiais[4] = materialResposta5;
        scriptInicializacao.step = -1;
    }

    void setMaterial(GameObject gameObject, Material material)
    {
        MeshRenderer[] renderers = gameObject.GetComponentsInChildren<MeshRenderer>();
        foreach (MeshRenderer r in renderers)
        {
            r.material = material;
        }
    }

    void resetMaterial(GameObject gameObject, Material oldMaterial, Material newMaterial)
    {
        MeshRenderer[] renderers = gameObject.GetComponentsInChildren<MeshRenderer>();
        foreach (MeshRenderer r in renderers)
        {
           if (r.material.color.Equals(oldMaterial.color)) {
                r.material = newMaterial;
            }
        }
    }

    // Update is called once per frame
    void Update () {
        if (scriptInicializacao.step == 0)
        {
            inicializarPerguntas();
        }

        if (Input.GetMouseButtonDown(0) && canAsnwer)
        {
            textRespostaCerta.SetActive(false);
            textRespostaErrada.SetActive(false);
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, 100.0f))
            {
                if (hit.transform.tag.Equals("Resposta"))
                {
                    estado = hit.transform.GetComponentInParent<Estado>();
                    //Quer dizer que a resposta é um estado, então prepara o script pra fazer a mágica
                    //de movimentar o objeto
                    if (estado != null)
                    {
                        canAsnwer = false;
                        estadoT = hit.transform;
                        origem = estadoT.position;
                        escalaOrigem = estadoT.localScale;
                        Vector3 escalaEstado = estado.destino.localScale;
                        escalaDestino = new Vector3(escalaOrigem.x * escalaEstado.x, escalaOrigem.y * escalaEstado.y, escalaOrigem.z * escalaEstado.z);
                        distance = 0;
                        regiao = null;
                    }

                    MeshRenderer renderer = hit.transform.GetComponentInParent<MeshRenderer>();
                    renderer.material = materialRespotaCerta;
                    textRespostaCerta.SetActive(true);
                    Pergunta pergunta = perguntas[perguntaAtual].GetComponentInChildren<Pergunta>();
                    if ((pergunta != null) && (pergunta.objetoResposta != null))
                    {
                        objetoResposta = pergunta.objetoResposta.gameObject;
                        objetoResposta.SetActive(true);
                    }
                    //Debug.Log("It works!");
                    audio.PlayOneShot(correto, 1);
                } else if (hit.transform.tag.Equals("RespostaRegiao")) {
                    regiao = hit.transform.GetComponentInParent<Regiao>();
                    //Quer dizer que a resposta é um estado, então prepara o script pra fazer a mágica
                    //de movimentar o objeto
                    if (regiao != null)
                    {
                        canAsnwer = false;
                        estadoT = hit.transform.parent.parent.transform;
                        origem = estadoT.position;
                        escalaOrigem = estadoT.localScale;
                        Vector3 escalaEstado = regiao.destino.localScale;
                        escalaDestino = new Vector3(escalaOrigem.x * escalaEstado.x, escalaOrigem.y * escalaEstado.y, escalaOrigem.z * escalaEstado.z);
                        distance = 0;
                        estado = null;
                    }

                    MeshRenderer[] renderers = hit.transform.parent.parent.GetComponentsInChildren<MeshRenderer>();
                    foreach (MeshRenderer r in renderers)
                    {
                        r.material = materialRespotaCerta;
                    }
                    textRespostaCerta.SetActive(true);
                    PerguntaRegiao pergunta = perguntas[perguntaAtual].GetComponentInChildren<PerguntaRegiao>();
                    if ((pergunta != null) && (pergunta.objetoResposta != null))
                    {
                        objetoResposta = pergunta.objetoResposta.gameObject;
                        objetoResposta.SetActive(true);
                    }
                    audio.PlayOneShot(correto, 1);
                } else if (hit.transform.tag.Equals("RespostaErrada"))
                {
                    MeshRenderer renderer = hit.transform.GetComponentInParent<MeshRenderer>();
                    renderer.material = materialRespotaErrada;
                    textRespostaErrada.SetActive(true);
                    perguntas[perguntaAtual].gameObject.SetActive(false);
                    perguntas[perguntaAtual].gameObject.SetActive(true);
                    audio.PlayOneShot(errado, 1);
                }
            }
        }
        else if (!canAsnwer)
        {
            if (Input.GetMouseButtonDown(0) && canBack) {
                getBack = true;
            } else if (!getBack && !canBack) {
                //Indo
                distance += Time.deltaTime / 1.5f; //Significa que quero levar 1 segundo e meio para chegar no destino;
                Vector3 v;

                if (regiao != null)
                {
                    v = regiao.destino.position;
                } else
                {
                    v = estado.destino.position;
                }
                estadoT.position = Vector3.Lerp(origem, v, distance);
                estadoT.localScale = Vector3.Lerp(escalaOrigem, escalaDestino, distance);
                if (estadoT.position == v)
                {
                    distance = 0;
                    canBack = true;
                    //Debug.Log("It works!");
                    //FIXME: Passar isso pro comando que executa depois de X milissegundos para não voltar
                    //no mesmo segundo que termina de avançar
                    /*
					if (!running){
						StartCoroutine("Wait");
					}
                    */
                }
            } else if (getBack) {
                //Voltando
                distance += Time.deltaTime / 1.5f; //Significa que quero levar 1 segundo e meio para chegar no destino;
                Vector3 v;

                if (regiao != null)
                {
                    v = regiao.destino.position;
                }
                else
                {
                    v = estado.destino.position;
                }
                estadoT.position = Vector3.Lerp(v, origem, distance);
                estadoT.localScale = Vector3.Lerp(escalaDestino, escalaOrigem, distance);
                if (estadoT.position == origem)
                {
                    getBack = false;
                    distance = 0;
                    canBack = false;
                    canAsnwer = true;
                    if (objetoResposta != null) {
                        objetoResposta.SetActive(false);
                        objetoResposta = null;
                    }
                    perguntas[perguntaAtual].gameObject.SetActive(false);                    
                    if ((perguntaAtual + 1) < perguntas.Length)
                    {
                        perguntaAtual++;
                        perguntas[perguntaAtual].gameObject.SetActive(true);
                        setMaterial(estadoT.gameObject, materiais[materialAtual]);
                        resetMaterial(brasil, materialRespotaErrada, defaultMaterial);
                        materialAtual = (materialAtual + 1) % materiais.Length;
                        if (perguntaAtual == 5)
                        {
                            setMaterial(brasil, defaultMaterial);
                        }
                    }
                }
            }
        }
    }

	IEnumerator Wait() {
		running = true;
		yield return new WaitForSeconds(1);
		getBack = true;
		distance = 0;
		running = false;
	}


}
