﻿using UnityEngine;
using System.Collections;

public class Pergunta : MonoBehaviour {

    public Transform brasil;
    public Transform resposta;
    public Transform objetoResposta;

	void OnEnable () {        
        Transform[] objects = brasil.GetComponentsInChildren<Transform>();

        foreach (Transform go in objects) {
            if (go.parent != null)
            {
                Transform t = go.parent.GetComponent<Transform>();
                
                if (t == resposta) {
                    go.tag = "Resposta";
                } else {
                    go.tag = "RespostaErrada";
                }                
            }
        }
	}
	
	// Update is called once per frame
	void Update () {
	}
}
