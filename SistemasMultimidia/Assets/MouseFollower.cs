﻿using UnityEngine;
using System.Collections;

public class MouseFollower : MonoBehaviour {

	// Use this for initialization
	void Start () {
       
    }

    // Update is called once per frame
    void Update() {
        var v3 = Input.mousePosition;
        v3.z = 2.0f;
        v3 = Camera.main.ScreenToWorldPoint(v3);
        this.transform.position = v3;

        if (Input.GetKeyDown("c")) {
            Cursor.visible = !Cursor.visible;
        }
    }
}
