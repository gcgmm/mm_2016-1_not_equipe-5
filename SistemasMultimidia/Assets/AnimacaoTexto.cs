﻿using UnityEngine;
using System.Collections;

public class AnimacaoTexto : MonoBehaviour {

    private int step;
    private Vector3 position;
    private Vector3 limitBack;
    private Vector3 limitFront;
    private float distance = 0;

    // Use this for initialization
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (step != 0)
        {
            if (step == 1)
            {
                distance += Time.deltaTime / 0.5f;
                transform.position = Vector3.Lerp(limitBack, limitFront, distance);

                if (transform.position.Equals(limitFront))
                {
                    distance = 0;
                    step = 2;
                }
            }
            else if (step == 2)
            {
                distance += Time.deltaTime / 0.5f;
                transform.position = Vector3.Lerp(limitFront, position, distance);
                if (transform.position.Equals(position))
                {
                    distance = 0;
                    step = 0;
                }
            }

        }

    }

    void OnEnable()
    {
        print(this.gameObject);
        if (position.Equals(new Vector3(0, 0, 0)))
        {
            
            if ((transform.GetComponent<Pergunta>() != null) || (transform.GetComponent<PerguntaRegiao>() != null))
            {
                // Se é uma pergunta do tipo região ou estado, atribui a posição correta em tempo de execução
                position = new Vector3(-7.35f, -2.51f, transform.position.z);
            }
            else {
                position = new Vector3(transform.position.x, transform.position.y, transform.position.z);
            }
            limitBack = new Vector3(position.x, position.y, position.z + 1);
            limitFront = new Vector3(position.x, position.y, position.z - 0.3f);
        }
        transform.position = new Vector3(limitBack.x, limitBack.y, limitBack.z);
        step = 1;
    }

}
