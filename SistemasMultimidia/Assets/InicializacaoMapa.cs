﻿using UnityEngine;
using System.Collections;

public class InicializacaoMapa : MonoBehaviour {

    public int step;
    private Vector3 position;
    private Vector3 limitBack;
    private Vector3 limitFront;
    private float distance = 0;

    // Use this for initialization
    void Start () {
	    
	}
	
	// Update is called once per frame
	void Update () {
        if (step != 0)
        {
            if (step == 1)
            {
                distance += Time.deltaTime / 2.0f;
                transform.position = Vector3.Lerp(limitBack, limitFront, distance);

                if (transform.position.Equals(limitFront))
                {
                    distance = 0;
                    step = 2;
                }
            } else if (step == 2)
            {
                distance += Time.deltaTime / 1.0f;
                transform.position = Vector3.Lerp(limitFront, position, distance);

                if (transform.position.Equals(position))
                {
                    step = 0;
                }
            }

        }

    }

    void OnEnable()
    {
        position = transform.position;
        limitBack = new Vector3(position.x, position.y, position.z + 20);
        limitFront = new Vector3(position.x, position.y, position.z - 0.5f);
        transform.position = limitBack;
        step = 1;      
    }
}
